



#ifndef TextDisplay_HPP
#define TextDisplay_HPP



#include "allegro5/allegro.h"
#include "allegro5/allegro_font.h"
#include "Eagle/backends/Allegro5Backend.hpp"



struct USTR_CHAR {
   
   int w;
   int h;
   ALLEGRO_USTR* uchar;
   
};



class TextDisplay {
   
protected :
   
   Allegro5Font* kudou_450;
   Allegro5Font* kudou_80;
   
   EagleGraphicsContext* win;
   
public :
   
   bool Init(EagleGraphicsContext* window);
   void DrawText(const char* japanese , const char* english , ALLEGRO_COLOR col);

   
};



#endif // TextDisplay_HPP

