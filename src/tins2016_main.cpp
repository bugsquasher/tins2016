






#include "Eagle.hpp"
#include "Eagle/backends/Allegro5Backend.hpp"
#include "allegro5/allegro_font.h"
#include "allegro5/allegro_ttf.h"

int main(int argc , char** argv) {
   
   (void)argc;
   (void)argv;
   
   EagleSystem* sys = new Allegro5System();
   
   
   if (EAGLE_FULL_SETUP != sys->Initialize(EAGLE_FULL_SETUP)) {
      return 1;
   }
   
   int ww = 800;
   int wh = 600;
   EagleGraphicsContext* win = sys->CreateGraphicsContext(ww,wh,EAGLE_OPENGL | EAGLE_WINDOWED);
   if (!win->Valid()) {
      return 2;
   }

   win->Clear(EagleColor(0,0,0));
   win->FlipDisplay();
   
   EagleImage* landscape = win->LoadImageFromFile("Data/Images/SnowyLandscape1600x900.jpg");
   
   Allegro5Font* kudou_font = dynamic_cast<Allegro5Font*>(win->LoadFont("Data/Fonts/Kudou.ttf" , -450 , LOAD_FONT_NO_KERNING));

   
      
   /// "\u96EA , \u7537"
   
   win->DrawStretchedRegion(landscape , 0 , 0 , landscape->W() , landscape->H() , 0 , 0 , ww , wh);

   char buf[1024];
   memset(buf , 0 , sizeof(char)*1024);
   
   int nbytes = al_utf8_encode(buf , 0x96EA);
   nbytes += al_utf8_encode(buf + nbytes , 0x7537);
   
   al_utf8_encode(buf + nbytes , '\0');
   
   ALLEGRO_USTR* ustr_title = al_ustr_new(buf);
///   al_ustr_append_chr(ustr_title , '\u96EA');
///   al_ustr_append_chr(ustr_title , '\u7537');
   
   int x,y,w,h;
   al_get_ustr_dimensions(kudou_font->AllegroFont() , ustr_title , &x , &y , &w , &h);
   
///   ALLEGRO_USTR* ustr = al_ustr_new("Hello USTR!");
   
///   al_draw_ustr(kudou_font->AllegroFont() , al_map_rgb(0,0,255) , 10 , 10 , 0 , ustr);
   
///   al_draw_ustr(kudou_font->AllegroFont() , al_map_rgb(255,0,0) , (ww - w)/2 , (wh - h)/2 , 0 , ustr_title);
   al_draw_ustr(kudou_font->AllegroFont() , al_map_rgb(255,0,0) , (ww - w)/2 , 0 , 0 , ustr_title);
   
   al_draw_text(kudou_font->AllegroFont() , al_map_rgb(255,127,0) , (ww - w)/2 , wh/2 , 0 , buf);
/**   
void al_draw_ustr(const ALLEGRO_FONT *font,
   ALLEGRO_COLOR color, float x, float y, int flags,
   const ALLEGRO_USTR *ustr) 
*/   
   win->FlipDisplay();
   
   sys->GetSystemQueue()->WaitForEvent(EAGLE_EVENT_KEY_DOWN);
   
///   sys->Rest(3.0);
   
   return 0;
}


