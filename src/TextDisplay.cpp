


#include "TextDisplay.hpp"



bool TextDisplay::Init(EagleGraphicsContext* window) {
   EAGLE_ASSERT(window);
   kudou_450 = window->LoadFont("Data/Fonts/Kudou.ttf" , -450);
   kudou_80 = window->LoadFont("Data/Fonts/Kudou.ttf" , -80);
   EAGLE_ASSERT(kudou_450 && kudou_450->Valid());
   EAGLE_ASSERT(kudou_80 && kudou_80->Valid());
   return kudou_450->Valid() && kudou_80->Valid();
}
   


void TextDisplay::DrawText(EagleGraphicsContext* win , const char* japanese , const char* english , ALLEGRO_COLOR col) {
   EAGLE_ASSERT(win);
   EAGLE_ASSERT(japanese);
   EAGLE_ASSERT(english);
   
   int jw = 0;
   int jh = 0;
   int ew = 0;
   int eh = 0;
   
   vector<USTR_CHAR> ustr_vec;
   
   ALLEGRO_USTR* jap_ustr = al_ustr_new_from_buffer(japanese);
   int jap_length = al_ustr_length(jap_ustr);
   for (int i = 0 ; i < jap_length ; ++i) {
      ALLEGRO_USTR* sub = al_ustr_dup_substr(jap_ustr , al_ustr_offset(jap_ustr , i) , al_ustr_offset(jap_ustr , i + 1));
      int x,y,w,h;
      al_get_ustr_dimensions(kudou_450 , sub , &x , &y , &w , &h);
      jw += w;
      jh = h;
      USTR_CHAR uchar;
      uchar.w = w;
      uchar.h = h;
      uchar.uchar = sub;
      ustr_vec.push_back(uchar);
   }
   
   ew = kudou_80->Width(english);
   eh = kudou_80->Height();
   
   int jtx = win->W()/2 - jw/2;
   int jty = win->H()/2 - (jh + eh)/2;
   int etx = win->W()/2 - ew/2;
   int ety = jty + jh;
   
   
   for (int i = 0 ; i < jap_length ; ++i) {
      ALLEGRO_USTR* sub = ustr_vec[i].uchar;
      al_draw_ustr(kudou_450 , col , jtx , jty , 0 , sub);
      jtx += ustr_vec[i].w;
      al_ustr_free(sub);
   }
   
   al_draw_text(kudou_80 , col , etx , ety , 0 , english);
   
   al_ustr_free(jap_ustr);
   
}
